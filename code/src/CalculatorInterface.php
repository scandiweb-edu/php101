<?php

interface CalculatorInterface
{
    public function calculate(string $input): int;

    public function convert(int $input, int $base): int;
}
 