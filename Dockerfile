FROM php:7.1
RUN apt-get update && apt-get install -y wget zip unzip git zlib1g-dev
RUN docker-php-ext-install zip
RUN cd /opt/ && wget https://phar.phpunit.de/phpunit.phar
RUN chmod +x /opt/phpunit.phar
RUN ln -s /opt/phpunit.phar /usr/local/bin/phpunit
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" &&\
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer &&\
    php -r "unlink('composer-setup.php');"
